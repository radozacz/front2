// Imports: Dependencies
const path = require('path');
require("babel-register");
const Dotenv = require('dotenv-webpack');
const dotenv = require('dotenv').config({path: __dirname + '/.env'});

console.log("ASd");

// Webpack Configuration
const config = {
  
  // Entry
  entry: './src/index.js',
  // Output
  output: {
    path: path.resolve(__dirname, process.env.DIST),
    filename: 'main.js',
  },
  // Loaders
  module: {
    rules : [
      // JavaScript/JSX Files
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      // CSS Files
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      }
    ]
  },
  watch: true,
  devServer: {
    contentBase: path.join(__dirname, process.env.DIST),
    compress: true,
    port: 9000
  },
  // Loaders
  // Plugins
  plugins: [],
};
// Exports
module.exports = config;
