import './scss/main.scss';
import './js/main.js';

import React, { Component } from "react";
import ReactDOM from "react-dom";

import App from './components/App/App';

document.addEventListener("DOMContentLoaded", function(){

});

let root = document.querySelector("#root");
ReactDOM.render(<App />, root);
