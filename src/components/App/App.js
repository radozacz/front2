import React, { Component } from "react";
import LoginForm from '../LoginForm/LoginForm';

class App extends Component {

    state = {
        header: null,
        primary: <LoginForm catchUser={this.catchUserHandler} />,
        footer: null,
        user: null
    }

    catchUserHandler( user ){
        console.log( 'catchUserHandler', user );
        console.log( this );
        //this.setUser( user );
    }

    setUser( user ){
        console.log( 'setUser', user );
        /*
        this.setState({
            user: user,
        });
        this.setHeader();
        */
    }

    setHeader(){
        this.setState({
            header: <h1>Czesc {this.state.user.name}</h1>
        });
    }

    dupaHandler(){
        /*this.setState({
            header: null,
            primary: null,
            footer: null
        });*/
        console.log("dupaHandler()");
    }
    
    render() {

        if( this.state.user ){
            this.setHeader();
        }

        return (
            <div className="page">
                <div>
                    {this.state.header}
                </div>
                <div>
                    {this.state.primary}
                </div>
                <div>
                    {this.state.footer}
                </div>
            </div>
        );
    }

}

export default App;
