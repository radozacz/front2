//remove starting Loader
function disableStarterLoader(){
    let styleLoader = document.querySelector("#style-site-loader");
    let loader = document.querySelector("#element-site-loader");
    if( styleLoader ){ styleLoader.remove(); }
    if( loader ){ loader.remove(); }
}

document.addEventListener("DOMContentLoaded", function(){
    disableStarterLoader();
});

