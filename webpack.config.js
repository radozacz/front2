// Imports: Dependencies
const path = require('path');
require("babel-register");
const Dotenv = require('dotenv-webpack');
const dotenv = require('dotenv').config({path: __dirname + '/.env'});

// Webpack Configuration
const config = {
  
  // Entry
  entry: './src/index.js',
  // Output
  output: {
    path: path.resolve(__dirname, process.env.DIST),
    filename: 'main.js',
  },
  // Loaders
  module: {
    rules : [
      // JavaScript/JSX Files
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      // CSS Files
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      }
    ]
  },
  // Plugins
  plugins: [],
};
// Exports
module.exports = config;
