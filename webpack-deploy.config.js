var path = require('path');
var WebpackFtpUpload = require('webpack-ftp-upload-plugin');
const Dotenv = require('dotenv-webpack');
var dotenv = require('dotenv').config({path: __dirname + '/.env'});
 
module.exports = {
    plugins: [
        new WebpackFtpUpload({
            host: process.env.DEPLOY_HOST,
            port: process.env.DEPLOY_PORT,
            username: process.env.DEPLOY_USER,
            password: process.env.DEPLOY_PASSWORD,
            local: path.join(__dirname, process.env.DIST),
            path: process.env.DEPLOY_PATH,
        })
    ]
}
